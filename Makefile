CPP=g++
OPT=-O3 -Wall -DNDEBUG
#OPT=-O0 -Wall -pg
INCLUDE=-I ~/program/libs/boost_1_53_0
LIBS=

ising2d.out: src/*.cpp src/*.hpp Makefile
	$(CPP) $(OPT) $(INCLUDE) src/main.cpp $(LIBS) -o $@

clean:
	rm -f *.out *~ *.bak
